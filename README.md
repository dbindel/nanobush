# NanoBush

This is a library of minor hackery, silly trickery, and odds and ends
that I normally wouldn't put in a library.  The name "NanoBush" is a
reference both to the design philosophy (little bushes vs big trees or
frameworks) and to Nanabush, a trickster god of the Anishinaabe
tribes, who most often appears as a rabbit.  In some cases, these are
not even software, but rather notes and advice that I considered
useful and was worried that I might forget (or forget to pass on when
asked).

All software here that I wrote is under an MIT license; and anything
that I did not write is appropriately documented.  Documents are
CC-BY.  Frankly, I probably won't even care if you forget to
acknowledge me.  I just want to be able to share the code and to keep
using it myself.

## Build system examples

The `buildex` subdirectory documents some recipes and advice for using
the CMake build system.  The most "real" examples are CMake build
systems added to a few existing libraries (see `buildex/tools`);
the toy examples in the `buildex/src` subdirectory mostly illustrate
how to use generators and build cross-language interfaces.  The
`buildex/cmake` subdirectory includes some modules not used by any
of the examples or tools, but generally useful.

## Third party tools

There are a number of very useful third-party packages that are
something of a pain to build.  Fortunately, CMake provides
"superbuild" options via external projects, and the build scripts in
this directory support this mode of operation.

## Documentation tools

`LDoc` is perhaps the world's stupidest code documentation processor.
Special comments turn the processor on and off; otherwise, marked
comment blocks turn into text and everything else is marked as code.
Everything fancy is pushed on the author or the formatting tool.
My recommendation is to write the documentation 
in [Pandoc][pandoc]-flavored [Markdown][markdown] and leave it with
all the work of producing pretty documents.  It would also be
straightforward to generate [reStructuredText][rest] (and then
perhaps to run the result through [Sphinx][sphinx]).

I'm fine with big documentation tools like [Doxygen][doxygen] 
or [Sphinx][sphinx], but in this case it's nice to have something small
and stupid.  Also, last I checked, Doxygen supports neither MATLAB nor
Lua, two languages that I like a lot.

  [pandoc]: http://johnmacfarlane.net/pandoc/
  [markdown]: http://daringfireball.net/projects/markdown/
  [rest]: http://docutils.sourceforge.net/rst.html
  [doxygen]: http://www.stack.nl/~dimitri/doxygen/
  [sphinx]: http://www.sphinx-doc.org

## Project components under development

There are a lot of odds and ends sitting around my drive that should
go here.  These will be added as my time, inclination, and needs
dictate.

### Further third-party tools

- [SUNDIALS](http://sundials.wikidot.com/)
- deal.II, Trilinos, and PETSc (pointers only)
- Building configuration files (CMake and pkgconfig) for thirdparty packages
- Cython example

### Basic numerical infrastructure

 - Basic sparse matrix libraries (or use Eigen, Armadillo?)
 - Matrix and vector I/O (including MATLAB, HB, etc)
 - Matrix assembly routines
 - H matrix operations?
 - Quadrature rules (including high-order)
 - Finite element shape functions / interpolation
 - Flexible heaps (for Dijkstra et al)
 - A little geometry stuff
 - Library of Krylov / Newton / etc solvers (at least Lanczos and Arnoldi)
 - Evaluation and manipulation of orthogonal polynomials
 - Chebyshev interpolant manipulation
 - Scalar and vector extrapolation routines
 - Pade approximation
 - Random numbers?

### Language tooling

 - Start from re2c + lemon -- both public domain, pretty simple,
   can include the whole kit and kaboodle
 - MWrap update (misc patches, MATLAB OO updates)
 - Matexpr update
 - Lsym update
 - VELCROS (Very Little Macro System) -- Lua macro preprocessing system
 - Lua mini-language development toolkit
   - Lua + Lemon = Lumen?
   - Use gmllua as an example
   - Goal is a mix-and-match workbench for embedded mini-languages
     (matexpr + special little languages for configuration, etc)
 - Interface generators for MATLAB (MEX or RPC), Lua, Python, Julia

### External interfaces

 - C++ interface to ARPACK (or arpack-ng) -- maybe something new?
 - Multi-language support for loading / saving MATLAB data (or HB, others?)
 - Lua interfaces
 - Python interfaces
 - Julia interfaces

### Plotting and visualization

 - Tutorials and demos for VTK output
 - Tutorials and demos for TikZ output

### Bare-bones applications

 - Fast multipole
 - Fast marching
 - Fast sweeping
 - Finite element solver

