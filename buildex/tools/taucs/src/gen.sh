cat > $1_G.c <<EOF
#define TAUCS_CORE_GENERAL
#include "$1.c"
EOF

cat > $1_D.c <<EOF
#define TAUCS_CORE_DOUBLE
#include "$1.c"
EOF

cat > $1_S.c <<EOF
#define TAUCS_CORE_SINGLE
#include "$1.c"
EOF

cat > $1_Z.c <<EOF
#define TAUCS_CORE_DCOMPLEX
#include "$1.c"
EOF

cat > $1_C.c <<EOF
#define TAUCS_CORE_SCOMPLEX
#include "$1.c"
EOF
