# Cross-compilation

I develop almost exclusively on OS X and Linux, but sometimes Windows
users would like to call my software.  One solution to this is to get
a Windows box (or set up a Windows virtual machine) that is only ever
used for building software.  As an alternative, I'm also setting up
(again) a cross-compile environment.

The [MXE (M cross environment)][mxe] is a system for building a
cross-compiler and many associated tools.  The MXE environment
explicitly supports CMake, and the CMake wiki provides 
a [tutorial description of how to cross-compile][wikimingw].
It's worth scanning even if you know how cross-compilers work,
since you may want to know how to use a generator in a
cross-compilation setting.

  [mxe]: http://mxe.cc/
  [wikimingw]: http://www.vtk.org/Wiki/CmakeMingw

## Notes

Qt has some issues compiling with non-Apple g++ variants under OS X
(because of the `-fconstant-cfstrings` flag).  The problem is that
even a cross-compile wants to build QMake and related tools, and the
default behavior on OS X is to assume those tools should be built with
Apple GCC, using Apple-specific compiler extensions that are normally
used to support Cocoa/Carbon programs.  My default compiler at the
time of this note is the MacPorts gcc4.7.  Hence, to build the MXE
system on my laptop, I had to first use

    sudo port select --set gcc apple-gcc42
    
On finishing the build, I will change back to my normal GCC choice.
