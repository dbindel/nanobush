# MEX files and external MATLAB interfaces

CMake has a [page on building MEX files with CMake][cmakemex].  This
was painful with autotools and hand-maintained Makefiles, and it
remains painful with CMake.  The problem lies not with CMake, but with
`mex`, which has been a recalcitrant beast throughout my long
acquaintance with it.  Note that MATLAB also has another mechanisms for
interacting with an external library: a shared library import system
built with some relation to MEX.

On Windows, one can either build with Visual Studio or with 
the [gnumex][gnumex] system.  The software is still apparently being
updated on SourceForge, but the documentation page is not.  Also,
gnumex only works for 32-bit Windows.  David Gleich has apparently
figured out how to [compile a 64-bit MEX file][dgmex] using the GNU
toolchain, but I have not tried this myself.

Given the pain of building and maintaining MEX files, another option
is to use MATLAB's Java interfaces to communicate over a socket or
other connection with some external server.  There are many RPC
mechanisms out there; one promising one might be [Apache Thrift][thrift].

  [cmakemex]: http://cmake.org/Wiki/CMake/MatlabMex
  [gnumex]: http://gnumex.sourceforge.net/
  [dgmex]: http://dgleich.wordpress.com/2012/01/08/compiling-mex-files-with-mingw64/
  [thrift]: http://thrift.apache.org/
