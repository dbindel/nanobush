# Build systems

The [CMake project][cmake] provides a transportable
build-and-configure system, similar in nature to Autotools but with
less black magic.  It has been adopted by the KDE project, deal.ii,
Trilinos, and VTK, and it is available as an experimental build option
for PETSc and Boost.  Because it is becoming widely used for large
numerical codes, it has several modules to manage linking with
standard numerical libraries.  It is my prefered build system at this
point.

The examples in this subdirectory illustrate some of the ways that
CMake can be used to build packages.  This includes builds with
generated files, builds of external tool chains, and generation of
SWIG interfaces.  The documentation for CMake is not fantastic,
but there are several useful references:

- [CMake Wiki](http://www.cmake.org/Wiki/CMake)
- [CMake Tutorial](http://www.cmake.org/cmake/help/cmake_tutorial.html) 
- [CMake Recipes from LCFG project](http://www.lcfg.org/doc/buildtools/cmake_recipes.html)
- [CMake intro for Swarthmore CS](http://www.cs.swarthmore.edu/~adanner/tips/cmake.php)
- [An empirical approach to CMake](http://rachid.koucha.free.fr/tech_corner/cmake_manual.html)
- [CMake intro from Ogre3D Wiki](http://www.ogre3d.org/wiki/index.php/Getting_Started_With_CMake)
- [Bruno Abinader's "How CMake cimplifies the build process"](http://brunoabinader.blogspot.com/2009/12/how-cmake-simplifies-build-process-part.html)
- [KDE TechBase: CMake](http://techbase.kde.org/Development/CMake)

For those running CMake as users rather than writing build new build
systems in CMake, I recommend the [Trilinos quick start][quick]
document, which gives a good introduction to best practices.  The
recommended way to use CMake is to build into a separate (non-source)
directory, with a script to record any parameter fiddling.  An example
of such a script is in the `sample_scripts` subdirectory.

Note that the current module for finding Python libraries seems to
get confused on some platforms (including mine).  It finds *a*
library, but not necessarily the one used by your Python version.
This can be dealt with by manually setting the `CMAKE_PREFIX_PATH`
variable; see the `sample_scripts` subdirectory.

 [cmake]: http://www.cmake.org/
 [quick]: http://trilinos.sandia.gov/Trilinos10CMakeQuickstart.txt

# Packaging

The [`CPack` system][cpack] automatically produces source or binary package
files.  To get the basic functionality, just include the line

    include(CPack)

in your `CMakeLists.txt` file.  I have only used this to build source
releases for things so far.  There are [several variables][cpackv]
that one might want to configure.  A reasonable minimum would seem to
be the description, vendor, and version number.

 [cpack]: http://www.cmake.org/Wiki/CMake:Packaging_With_CPack
 [cpackv]: http://www.cmake.org/Wiki/CMake:CPackConfiguration

# Testing

The [`CTest` system][ctest] is a test automation framework.  It can be
used either through the Make system (`make test`) or through a
dashboard like [CDash][cdash] or [Dart][dart].  For a larger project,
it likely makes sense to set up such a dashboard; note that Kitware
offers [free hosting for dashboards][mycdash].  I have not yet tried
this out for myself.

To add a simple test to the build system, include `CTest` and use the
`add_test(test-name command args)` command:

    include(Ctest)
    add_test("Test-foobar-1" test_foobar 1)
    
where `test_foobar` is an executable compiled by CMake.  The test is
judged to succeed or fail based on the return code of the executable.

The `add_test` command only accepts a single executable, but that
can be *any* executable.  In particular, that means that one
can [recursively call CMake in order to run portable tests][q1].
In particular, this mechanism can be used to
portably [diff results against reference outputs][q2].  Note that
in order to get this to work, it is probably necessary to use
explicitly-named `NAME` and `COMMAND` arguments to `add_test`.
For example, the following command runs a test that writes a file
to standard output, then does a diff against a reference file:

    add_test(NAME test-mylua-stdout
             COMMAND ${CMAKE_COMMAND} 
               -DCMD=$<TARGET_FILE:mylua>
               -DOUT=$<TARGET_FILE_DIR:mylua>/hello.txt
               -DARG=${CMAKE_SOURCE_DIR}/test/exlua/testref.lua
               -DREF=${CMAKE_SOURCE_DIR}/test/exlua/hello.txt
               -P ${CMAKE_SOURCE_DIR}/test/diffout.cmake)

Note the use of generator expressions (e.g. `$<TARGET_FILE:mylua>`) in
the above command.  We cannot do this unless we use the `COMMAND` form.

 [ctest]: http://cmake.org/Wiki/CMake/Testing_With_CTest
 [cdash]: http://public.kitware.com/Wiki/CDash:FAQ
 [dart]: http://public.kitware.com/Dart/HTML/Index.shtml
 [mycdash]: http://my.cdash.org/
 [q1]: http://stackoverflow.com/questions/3065220/ctest-with-multiple-commands
 [q2]: http://www.cmake.org/pipermail/cmake/2009-July/030619.html
  
# Installation

CMake provides ["an elaborate installation system"][cmakein] which
nonetheless seems reasonably straightforward: identify the libraries,
binaries, and support files that you want to install, and CMake will
put them in the right place.  If you want to install something like
a Python module, you might be better off using Python distutils;
fortunately, [CMake can also help with that][pyin].

In some cases, it may make sense to install a script that is generated
at configuration time.  An example is given with the `ldoc` tool at
the top level.

 [cmakein]: http://www.cmake.org/Wiki/CMake:Install_Commands
 [pyin]: http://stackoverflow.com/questions/13298504/using-cmake-with-setup-py

# Shared and static libraries

One of the areas where systems like CMake and autoconf shine is in
packaging of shared libraries.  The three most common desktop
operating system families (Linux, OS X, and Windows) all have
rather different way of managing shared libraries.  Under Linux and
OS X, dynamic libraries consist of a single file with the ending
`.so` (Linux) or `.dylib` (OS X).  Under Windows, there are typically
two libraries: a static import library, which has the `.lib`
extension, and a dynamic library (`.dll`).  Both the import library
and the dynamic library can be generated by CMake, and exported functions
in this environment are written with a special decoration; the
`generate_export_header` macro helps with this,
as [described on the CMake wiki pages][wiki-dllexp].
Alternately, one can provide a `.def` file with a list of exported
functions; see the `buildex/tools/lua` example.

In Linux and OS X, it is common to have both a shared and a static
version of the same library, differing only in the extension.  In
Windows, this is more problematic, because there may be a name
conflict between a static version of the library and an import
library.  Thus, you *cannot* generally ask CMake to write a shared
library and a static library with the same identifier.  What you *can*
do is either make a choice between shared or static libraries based on
a command line option or build both a static and a shared library, but
with different names.  In the latter case, you will need to tell CMake
that it should not try to re-use the object files used in building the
static library to build the shared library -- one is typically build
without `-fPIC` and the other with, and so the two are not generally
compatible.  The `CLEAN_DIRECT_OUTPUT` directive essentially says
"clean up all the intermediate files in building this library" so that
one doesn't clobber the shared library compile with the static
compile" (and vice-versa).  See the Lua and tolua++ build files to see
how this works.

One would sometimes like to build a dynamic library that links in
routines from a static library.  In order to do this under Linux,
the static library must be compiled with the `-fPIC` flag to
enable generation of Position Independent Code.  This doesn't happen
by default with CMake, so if you decide you really want a static
library that can be used in this way, you should manually add
the `-fPIC` flag (e.g. by setting `CMAKE_C_FLAGS` when invoking
the `CMake` command).

For building shared libraries, it is also helpful to understand
how [CMake handles RPATHs][rpath].

  [rpath]: http://www.cmake.org/Wiki/CMake_RPATH_handling
  [wiki-dllexp]: http://www.cmake.org/Wiki/BuildingWinDLL

# Notes

In order to link against Fortran, you need `Fortran` in your language
list.  This is true even if you just are using a Fortran library that
requires the associated runtime.

The `exlua` example shows how to produce custom commands associated
with generators.

In debug mode, everything is compiled *without* optimizations.  The
release mode C flags include `-O3` and `-DNDEBUG`, which is better for
performance.


