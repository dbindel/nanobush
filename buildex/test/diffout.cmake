#
# Set path if we're running a Python tester
#
if (PYTHONPATH)
  message("Python path: ${PYTHONPATH}")
  set(ENV{PYTHONPATH} "${PYTHONPATH}")
endif ()

#
# Run the command and redirect output
#
message("Running: ${CMD} ${ARG}")
execute_process(COMMAND ${CMD} ${ARG}
                RESULT_VARIABLE CMD_RESULT
                OUTPUT_FILE ${OUT}
                OUTPUT_STRIP_TRAILING_WHITESPACE)

#
# Check to make sure we didn't fail miserably
#
if (CMD_RESULT)
  message(FATAL_ERROR "Error running ${CMD}")
endif (CMD_RESULT)

#
# Check output matches desired
#
if (REF)
  message("Diffing ${OUT} ${REF}")
  execute_process(COMMAND ${CMAKE_COMMAND} -E compare_files ${OUT} ${REF}
                  RESULT_VARIABLE DIFFERENT)
  if (DIFFERENT)
    message(FATAL_ERROR "Test failed- files differ")
  endif (DIFFERENT)
endif(REF)
