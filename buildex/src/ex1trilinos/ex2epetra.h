#ifndef EX2EPETRA_H
#define EX2EPETRA_H

#include "Epetra_SerialDenseVector.h"
double epetra_two_norm(Epetra_SerialDenseVector& x);

#endif /* EX2EPETRA_H */

