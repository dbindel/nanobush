#include "ex2epetra.h"

double epetra_two_norm(Epetra_SerialDenseVector& x)
{
    return x.Norm2();
}

