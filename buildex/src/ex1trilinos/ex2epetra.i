/* File : ex2epetra.i */
%module ex2epetra
%{
/* Put headers and other declarations here */
#include "ex2epetra.h"
%}

double epetra_two_norm(Epetra_SerialDenseVector& x);
