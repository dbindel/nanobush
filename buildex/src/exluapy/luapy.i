/* File : luapy.i */
%module luapy
%{
/* Put headers and other declarations here */
extern "C" {
#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
}
%}

typedef struct lua_State lua_State;

/*
** prototype for memory-allocation functions
*/
typedef void * (*lua_Alloc) (void *ud, void *ptr, size_t osize, size_t nsize);

/*
** state manipulation
*/
lua_State *lua_newstate(lua_Alloc f, void *ud);
lua_State *lua_open();
void       lua_close(lua_State *L);
lua_State *lua_newthread(lua_State *L);

/* Auxiliary libraries */
void luaL_openlibs(lua_State* L);
int  luaL_loadfile(lua_State *L, const char *filename);
int  luaL_loadstring(lua_State *L, const char *s);
void luaL_dofile(lua_State* L, const char* fn);
void luaL_dostring(lua_State* L, const char* s);

/*
** basic stack manipulation
*/
int   lua_gettop (lua_State *L);
void  lua_settop (lua_State *L, int idx);
void  lua_pushvalue (lua_State *L, int idx);
void  lua_remove (lua_State *L, int idx);
void  lua_insert (lua_State *L, int idx);
void  lua_replace (lua_State *L, int idx);
int   lua_checkstack (lua_State *L, int sz);

void  lua_xmove (lua_State *from, lua_State *to, int n);

/*
** access functions (stack -> C)
*/

int             lua_isnumber(lua_State *L, int idx);
int             lua_isstring(lua_State *L, int idx);
int             lua_iscfunction(lua_State *L, int idx);
int             lua_isuserdata(lua_State *L, int idx);
int             lua_type(lua_State *L, int idx);
const char     *lua_typename(lua_State *L, int tp);

int            lua_equal(lua_State *L, int idx1, int idx2);
int            lua_rawequal(lua_State *L, int idx1, int idx2);
int            lua_lessthan(lua_State *L, int idx1, int idx2);

double          lua_tonumber(lua_State *L, int idx);
int             lua_tointeger(lua_State *L, int idx);
int             lua_toboolean(lua_State *L, int idx);
const char     *lua_tolstring(lua_State *L, int idx, size_t *len);
size_t          lua_objlen(lua_State *L, int idx);
void	       *lua_touserdata(lua_State *L, int idx);
lua_State      *lua_tothread(lua_State *L, int idx);
const void     *lua_topointer(lua_State *L, int idx);


/*
** push functions (C -> stack)
*/
void  lua_pushnil(lua_State *L);
void  lua_pushnumber(lua_State *L, double n);
void  lua_pushinteger(lua_State *L, int n);
void  lua_pushlstring(lua_State *L, const char *s, size_t l);
void  lua_pushstring(lua_State *L, const char *s);
void  lua_pushboolean(lua_State *L, int b);
int   lua_pushthread(lua_State *L);


/*
** get functions (lua -> stack)
*/
void  lua_gettable(lua_State *L, int idx);
void  lua_getfield(lua_State *L, int idx, const char *k);
void  lua_rawget(lua_State *L, int idx);
void  lua_rawgeti(lua_State *L, int idx, int n);
void  lua_createtable(lua_State *L, int narr, int nrec);
void *lua_newuserdata(lua_State *L, size_t sz);
int   lua_getmetatable(lua_State *L, int objindex);
void  lua_getfenv(lua_State *L, int idx);


/*
** set functions (stack -> Lua)
*/
void  lua_settable(lua_State *L, int idx);
void  lua_setfield(lua_State *L, int idx, const char *k);
void  lua_rawset(lua_State *L, int idx);
void  lua_rawseti(lua_State *L, int idx, int n);
int   lua_setmetatable(lua_State *L, int objindex);
int   lua_setfenv(lua_State *L, int idx);


/*
** `load' and `call' functions (load and run Lua code)
*/
void  lua_call(lua_State *L, int nargs, int nresults);
int   lua_pcall(lua_State *L, int nargs, int nresults, int errfunc);


/*
** coroutine functions
*/
int  lua_yield(lua_State *L, int nresults);
int  lua_resume(lua_State *L, int narg);
int  lua_status(lua_State *L);
