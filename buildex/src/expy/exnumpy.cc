#include <iostream>
#include "exnumpy.h"

double sum_elements(int len, double* x)
{
    double result = 0;
    for (int i = 0; i < len; ++i)
        result += x[i];
    return result;
}

