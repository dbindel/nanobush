/* File : exnumpy.i */
%module exnumpy
%{
#define SWIG_FILE_WITH_INIT
#include "exnumpy.h"
%}

%include "numpy.i"
%init %{
import_array();
%}

%apply (int DIM1, double* IN_ARRAY1)  {(int len, double* x)}

double sum_elements(int len, double* x);

