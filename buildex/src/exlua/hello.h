#ifndef HELLO_H
#define HELLO_H

class HelloClass {
public:
    HelloClass(int data) : data(data) {}
    int get_data() { return data; }
private:
    int data;
};

void hello_world();

#endif /* HELLO_H */
