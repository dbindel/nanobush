#
# UMFPACK
#

find_path( UMFPACK_INCLUDE_DIR umfpack.h 
           ${UMFPACK_ROOT}/include /usr/include /usr/include/umfpack 
           /usr/local/include/UMFPACK /usr/include/suitesparse 
           /opt/local/include/ufsparse )
find_path( AMD_INCLUDE_DIR amd.h 
           ${AMD_ROOT}/include /usr/include 
           /usr/local/include/AMD 
           /usr/include/suitesparse 
           /opt/local/include/ufsparse)

if (MSVC)
    set( UMFPACK_LIBRARY_NAME libumfpack )
    set( AMD_LIBRARY_NAME     libamd )
else(MSVC)
    set( UMFPACK_LIBRARY_NAME umfpack )
    set( AMD_LIBRARY_NAME     amd )
endif(MSVC)

find_library( UMFPACK_LIBRARY ${UMFPACK_LIBRARY_NAME} 
              ${UMFPACK_ROOT}/lib /usr/lib /usr/local/lib/UMFPACK 
              /opt/local/lib )
find_library( AMD_LIBRARY ${AMD_LIBRARY_NAME} 
              ${AMD_ROOT}/lib /usr/lib /usr/local/lib/AMD 
              /opt/local/lib )

include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( UMFPACK DEFAULT_MSG UMFPACK_INCLUDE_DIR 
                                   AMD_INCLUDE_DIR UMFPACK_LIBRARY AMD_LIBRARY)
