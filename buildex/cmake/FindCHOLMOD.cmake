#
# CHOLMOD
#

find_path( CHOLMOD_INCLUDE_DIR cholmod.h 
           ${CHOLMOD_ROOT}/include /usr/include /usr/include/cholmod
           /usr/local/include/CHOLMOD /usr/include/suitesparse 
           /opt/local/include/ufsparse )
find_path( AMD_INCLUDE_DIR amd.h 
           ${AMD_ROOT}/include /usr/include 
           /usr/local/include/AMD 
           /usr/include/suitesparse 
           /opt/local/include/ufsparse)

if (MSVC)
    set( CHOLMOD_LIBRARY_NAME libcholmod )
    set( AMD_LIBRARY_NAME     libamd )
else(MSVC)
    set( CHOLMOD_LIBRARY_NAME cholmod )
    set( AMD_LIBRARY_NAME     amd )
endif(MSVC)

find_library( CHOLMOD_LIBRARY ${CHOLMOD_LIBRARY_NAME} 
              ${CHOLMOD_ROOT}/lib /usr/lib /usr/local/lib/CHOLMOD 
              /opt/local/lib )
find_library( AMD_LIBRARY ${AMD_LIBRARY_NAME} 
              ${AMD_ROOT}/lib /usr/lib /usr/local/lib/AMD 
              /opt/local/lib )

include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( CHOLMOD DEFAULT_MSG CHOLMOD_INCLUDE_DIR 
                                   AMD_INCLUDE_DIR CHOLMOD_LIBRARY AMD_LIBRARY )
