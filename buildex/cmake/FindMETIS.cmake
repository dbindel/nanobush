#
# METIS
#

find_path( METIS_INCLUDE_DIR metis.h 
           ${METIS_ROOT}/include /usr/include 
           /usr/include/metis /usr/local/include/metis 
           /opt/local/include/metis )

if (MSVC)
    set( METIS_LIBRARY_NAME libmetis )
else(MSVC)
    set( METIS_LIBRARY_NAME metis )
endif(MSVC)

find_library( METIS_LIBRARY ${METIS_LIBRARY_NAME} 
              ${METIS_ROOT}/lib /usr/lib 
              /usr/local/lib/METIS
              /opt/local/lib)

include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( METIS DEFAULT_MSG METIS_INCLUDE_DIR )
