BUILDEX_SRC_DIR=../buildex
CANOPY=$HOME/Library/Enthought/Canopy_64bit/User/
export CMAKE_PREFIX_PATH=$CANOPY/lib

cmake \
  -D CMAKE_INSTALL_PREFIX=/tmp/test \
  -D CMAKE_BUILD_TYPE=Release \
  -D USE_TAUCS=ON \
  $@ \
  $BUILDEX_SRC_DIR
