BUILDEX_SRC_DIR=../buildex
cmake \
  -D CMAKE_INSTALL_PREFIX=$HOME/tmp/nonsense \
  -D CMAKE_BUILD_TYPE=Release \
  -D USE_TAUCS=ON \
  $@ \
  $BUILDEX_SRC_DIR
