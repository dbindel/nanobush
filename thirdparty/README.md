# Third-party packages

There are a number of very useful third-party packages that are
something of a pain to build.  Fortunately, CMake provides 
"superbuild" options via [external projects][eptut].  This sort of
superbuild is available for [ParaView][pvs], and my figuring out what
goes where was strongly influenced by [work of Marcus Hanwell][avos].

To build a package, CMake downloads a source distribution file (a zip file
or tarball, typically) and calls a `patch.cmake` file from the
`patches` subdirectory in order to apply any patches that are needed,
such as a patch to install a CMake-based build system.  It is also
possible to use the original configuration system with the package if
there is one; for several of these packages, there is a manual
`Makefile.in` configuration or something based on autotools.

In the case of SuiteSparse, we need to actually patch one of the files
if we want compatibility with the most recent version of METIS.
The actual process of calling the `patch` command is apparently a little 
tricky because of (you guessed it) the behavior of Windows.  There 
is [a workaround][patch] if needed.

 [eptut]: http://www.kitware.com/media/html/BuildingExternalProjectsWithCMake2.8.html
 [pvs]: http://paraview.org/Wiki/ParaView/Superbuild
 [avos]: http://www.cryos.net/archives/248-CMake-External-Projects-Building-Project-Dependencies.html
 [patch]: http://www.cmake.org/pipermail/cmake/2009-November/033528.html

# Available packages

## ARPACK

The [ARPACK-ng][arpack-ng] project maintains the latest and greatest
version of the Fortran ARPACK eigensolver library.  It is available
under a permissive BSD-style license.

 [arpack-ng]: http://forge.scilab.org/index.php/p/arpack-ng/

## Lua

The [Lua][lua] language is an small embeddable scripting languages
that I use in several of my projects.  It is available under an MIT
license.

  [lua]: http://www.lua.org/

## METIS 4.0

The [METIS][metis] library is a standard package for graph
partitioning and for computing fill-reducing orderings.  The most
recent version is METIS 5.1.0.  We include METIS 4.0 mostly because it
is still used as an (optional) dependency in SparseSuite.  This
version of METIS "can be freely used for educational and research
purposes by non-profit institutions and US government agencies
only. Other organizations are allowed to use METIS only for evaluation
purposes, and any further uses will require prior approval."  

  [metis]: http://glaros.dtc.umn.edu/gkhome/metis/metis/overview

## METIS 5.1

We now have an auto-patcher for SuiteSparse so that it can use either METIS 4
or METIS 5.  While Tim Davis is not yet officially supporting METIS 5, if you
want to use SuiteSparse along with any other software that uses the current
version of METIS, you really want to use this patch.  As of version 5.0.3,
METIS is distributed under the Apache License Version 2.0.

## SuiteSparse

The [SuiteSparse][suitesparse] library is a collection of sparse
matrix factorizations and related routines developed by Tim Davis and
his collaborators at the University of Florida.  The modules in this
library are licensed under a mix of GPL and LGPL.  There has already
been another [port of the build system to CMake][ss-for-win] for
Windows that I found after writing this build system; I have not tried
the latter, since I mostly do not use Windows.

Configuring the SuiteSparse package with `USE_METIS4` or `USE_METIS5`
causes CHOLMOD to use METIS.

  [suitesparse]: http://www.cise.ufl.edu/research/sparse/SuiteSparse/
  [ss-for-win]: https://code.google.com/p/suitesparse-metis-for-windows/

## Eigen

The [Eigen][eigen] library is a C++ header package that provides a
clean-looking linear algebra interface using template metaprogramming.
The library is licensed under the Mozilla Public License 2.0, a simple
copyleft that applies on a file-by-file basis; you can use it in
codes with different licenses, you just have to share changes that are
directly made to the Eigen files.

  [eigen]: http://eigen.tuxfamily.org/

## Armadillo

The [Armadillo][armadillo] library is another C++ matrix library,
sitting atop LAPACK/BLAS.  The library is licensed under the Mozilla
Public License 2.0.

  [armadillo]: http://arma.sourceforge.net/

## re2c

The [re2c][re2c] translator generates fast C scanners using an embedded
mini-language.  Apart from being fast and compact, re2c generates
scanners that are not tied to any particular input model, and it does
not pollute the name space with global variables (unlike flex).
re2c is in the public domain.

  [re2c]: http://re2c.org/
