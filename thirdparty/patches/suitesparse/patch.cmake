#
# Copy over the patch file
#
execute_process(
  COMMAND ${CMAKE_COMMAND} -E copy 
  ${PATCH}/CMakeLists.txt ${SRC}/CMakeLists.txt)

execute_process(
  COMMAND ${CMAKE_COMMAND} -E copy 
  ${PATCH}/cmake/FindMETIS.cmake ${SRC}/cmake/FindMETIS.cmake)

if (USE_METIS5)
  # FIXME: We should probably look up the patch executable
  set(PATCH_EXE patch)
  execute_process(COMMAND 
    ${CMAKE_COMMAND} -E chdir ${SRC}
    ${PATCH_EXE} -p1 -i ${PATCH}/metis5.patch
    RESULT_VARIABLE RESULT_PATCH)
  MESSAGE( STATUS "From dir ${PATCH} try${PATCH_EXE} -p1 -i ${SRC}/metis5.patch" )
  if (PATCH_RESULT)
    MESSAGE( FATAL_ERROR "Failed patch" )
  endif()
endif()

set(SUITESPARSE_MODULES
    AMD      COLAMD    SPQR
    BTF      CSparse   SuiteSparse_config
    CAMD     CXSparse  UMFPACK
    CCOLAMD  KLU       RBio
    CHOLMOD  LDL)

foreach (module ${SUITESPARSE_MODULES})
  execute_process(
    COMMAND ${CMAKE_COMMAND} -E copy 
    ${PATCH}/${module}/CMakeLists.txt ${SRC}/${module}/CMakeLists.txt)
endforeach()
