% LDOC(1) Ldoc User Manual
% David Bindel
% August 18, 2013

# NAME

ldoc - a simple literate programming tool

# SYNOPSIS

ldoc [*options*] [*input-files*]

# DESCRIPTION

LDoc generates extracts documentation from inline comments in comments
in code written in Lua, C, C++, Matlab, shell, Python, or languages
with compatible comment syntax.

Output goes to *stdout* by default.  For output to a file, use the `-o`
option:

    ldoc -o foo.md foo.c

The input and output formats may be specified using command-line
options (see **OPTIONS**, below, for details).  If the input format is
not specified explicitly, LDoc will attempt to determine it from the
extensions of the input filenames.  The output is assumed to be
Markdown unless otherwise specified.

The `ldoc` documentation tool is toggled on or off with special
comment lines consisting of the text `ldoc`.  Comments with just the
text `ldoc on` or `ldoc off` turn documentation on or off; `ldoc` with
nothing else toggles the current state.  By default, the documentation
is assumed to be turned off.  When documentation is turned on, text in
specified block comments is sent directly to the output, while text
outside block comments is formatted as code.  In C-family languages,
block comments beginning with two starts (`/**`) are treated as the
beginning of documentation blocks.  In Matlab, double comment blocks
that begin with double `%%` are documentation blocks; in shell,
it is comment blocks that begin with `##`.  In Lua,
documentation blocks begin with `--[[` and end with `--]]`.

# OPTIONS

-p *FORMAT*
:   Specify output file format.  *FORMAT* can be
    `markdown` (generic Markdown), `pandoc` (Pandoc-flavored
    Markdown), `github` (Github-flavored Markdown),
    `rest` (reStructuredText), or `latex` (LaTeX).

-l *LANGUAGE*
:   Specify an input language.  *LANGUAGE* can be
    `c` (a C-family language), `lua` (Lua), `m` (Matlab),
    or `sh` (a language with shell-style comments)
    
-o *FILE*, \--output=*FILE*
:   Write output to *FILE* instead of *stdout*.  If *FILE* is
    \``-`', output will go to *stdout*.
