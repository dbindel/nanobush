#
# Run the command and redirect output
#
separate_arguments(ARG)
message(STATUS ${ARG})
execute_process(COMMAND ${LUA_EXECUTABLE} ${LDOC} ${ARG} -o ${OUT} ${INP}
                RESULT_VARIABLE CMD_RESULT)

#
# Check to make sure we didn't fail miserably
#
if (CMD_RESULT)
  message(FATAL_ERROR "Error running ${CMD}")
endif (CMD_RESULT)

#
# Check output matches desired
#
if (REF)
  message("Diffing ${OUT} ${REF}")
  execute_process(COMMAND ${CMAKE_COMMAND} -E compare_files ${OUT} ${REF}
                  RESULT_VARIABLE DIFFERENT)
  if (DIFFERENT)
    message(FATAL_ERROR "Test failed- files differ")
  endif (DIFFERENT)
endif(REF)
