# ldoc: A Lua documentation tool

The `ldoc` tool converts a Lua code intermixed with text documentation
(in block comments) into a markup language.  The simplest literate
programming tool I will ever use, I expect.  Half the reason for
including this in `nanobush` is to illustrate build system features.

Assuming you have Lua installed, type `make` to generate `ldoc.md`
from the `ldoc.lua` source.  If you have Pandoc installed,
you can also try `make html`.
