# Licensing and copyright

I want people to use my software without consulting a lawyer.  But
software without any copyright or licensing information is considered
rightly dangerous by most firms, since the default behavior is that
all rights are reserved by the author.

I generally find copyright and licensing issues confusing.  Dammit,
Jim, I'm a Ph.D., not a J.D.!  But as some level of self-defense seems
to be necessary, I have done some reading.  Two good references have
been Fogel's [book on FOSS][fogel] and 
St. Laurent's [Understanding Open Source and Free Software Licensing][laurent].

 [fogel]: http://producingoss.com/
 [laurent]: http://oreilly.com/openbook/osfreesoft/book/index.html

## Software licenses (MIT)

There are many [open source license][osi] available.  While I'd prefer
to be acknowledged, I do not mind others using my work in commercial
products, and so I prefer to release software under
the [MIT license][mit].  The GitHub folks have an
an [article about licensing][git] that covers the basics;
they [recommend MIT][cal] for "simple" licensing.  This is also
the [recommendation][fogellic] in [Fogel's book on FOSS][fogel].

The point of using a permissive license is that it is allowed to
be used in other projects.  In particular, the MIT license (among others)
is [compatible with GPL according to the Free Software Foundation][gplmit].
This means that MIT-licensed code can be used in a GPL project.
The converse is not true, and so a released package that includes both
MIT-licensed and GPL-licensed components must be released under GPL.
The Software Freedom Foundation has [guidelines][gplcollab] regarding
the use of non-GPL software inside GPL projects which spells out the
issues with reasonable clarity.

 [osi]: http://opensource.org
 [mit]: http://opensource.org/licenses/MIT
 [git]: https://help.github.com/articles/open-source-licensing
 [cal]: http://choosealicense.com/
 [fogellic]: http://producingoss.com/en/license-choosing.html
 [gplmit]: http://www.gnu.org/licenses/license-list.html#Expat
 [gplcollab]: http://www.softwarefreedom.org/resources/2007/gpl-non-gpl-collaboration.html

## Media licenses ([CC BY 3.0][cc-by-3])

The [Creative Commons][cc] project maintains a set of licenses and
tools for sharing media content.  As noted in [their FAQ][ccfaq], the
Creative Commons licenses are *not* good replacements for software
licenses; but they are useful for documentation and media, which often
accompanies software projects.  My prefered default is the most
liberal of the non-public-domain options (CC BY), which only requires
attribution.

 [cc]:  http://creativecommons.org/
 [ccfaq]: http://wiki.creativecommons.org/Frequently_Asked_Questions
 [cc-by-3]: http://creativecommons.org/licenses/by/3.0/
 
## License and copyright scoping

The [Software Freedom Foundation][swf] has a white paper
on [managing copyright information][swf-where].
The authors of this paper distinguish between file-level
and centralized (or semi-centralized) copyright notices.
One of the chief points of the white paper is that a modern
version control system is better than manual annotation at keeping
track of who touched what module when, and suggests that file-level
copyrights can in many places be either omitted or replaced with
a notice of the form

> Copyright 2012 The Foo Project Developers. See the COPYRIGHT file at
> the top-level directory of this distribution and at
> http://example.org/project/COPYRIGHT.

A single top-level `AUTHORS` or `COPYRIGHT` form would then contain
the summary of contributions from the different authors.

In addition to copyright notices, licenses can be maintained at a file
scope or in a centralized location (e.g. a `COPYING` or `LICENSE`
file).  Individual files would then include a comment pointing to the
license.  In the white paper, the authors recommend something like:

> This file is part of Foo Project. It is subject to the license terms
> in the LICENSE file found in the top-level directory of this
> distribution and at http://www.example.org/foo/license.html. No part
> of Foo Project, including this file, may be copied, modified,
> propagated, or distributed except according to the terms contained in
> the LICENSE file.

 [swf]: http://softwarefreedom.org/
 [swf-where]: http://softwarefreedom.org/resources/2012/ManagingCopyrightInformation.html

## Contributor agreements

Along with his [discussion of licenses][fogellic], Fogel also
discusses [contributor agreements][fogelca] in his book.  Options here
include doing nothing (my preference!), contributor license agreements
(if needed), and copyright assignment.

If legal self-defense is needed, a [contributor license agreement][wikica] is
an agreement that says a project can redistribute contributed code.
With a CLA, the contributor retains copyright, but promises not to bop
the project over the head with it.  [Project Harmony][harmony]
provides templates and an auto-generator for contributor agreements.

 [fogelca]: http://producingoss.com/en/contributor-agreements.html
 [wikica]: http://wiki.civiccommons.org/Contributor_Agreements
 [harmony]: http://harmonyagreements.org/
